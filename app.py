from flask import Flask, jsonify, request
app = Flask(__name__)
import importlib
import numpy as np
import pymongo
import requests
from sklearn import svm

main = pymongo.MongoClient('localhost')["PYTHONCLASS2020"]["main"]
my = pymongo.MongoClient('localhost')["PYTHONCLASS2020"]["xlxukx"]


@app.route('/')
def hello():
	return "Hello, dear user!"

@app.route('/echo')
def echo():
        return request.args.get("echostring")

@app.route('/calc', methods=["POST","GET"])
def calc():
	#req=request.get_json()
	try:
		reqa=int(request.args.get("a"))
		reqb=int(request.args.get("b"))
		return {'sum': reqa + reqb, 'div': reqa / reqb}
	except TypeError:
		req=request.get_json()
		return {'sum': int(req["a"]) + int(req["b"]), 'div': int(req["a"]) / int(req["b"])}


@app.route('/fasta', methods=["POST","GET"])
def fasta():
	req=request.get_json()
	req1=str(req["source"])
	req2=str(req["function"])
	req3=str(req["arg"])
	fastalib = importlib.import_module("xlxukx_assignment1")
	ff = requests.get(req1)
	with open("proteins1.fasta", 'wb') as f:
		f.write(ff.content)
	proteins = fastalib.load_fasta("proteins1.fasta")
	#return {req1 : req2, req3:"dsfdsfds"}
	pc = len(proteins)
	if req2 == "num_with_motif":
		szam = fastalib.find_protein_with_motif(proteins, req3)
		szam = len(szam)
		return {"retval" : szam, "num_proteins" : pc}
	elif req2 == "num_amino_acids":
		szam  = 0
		for p in proteins:
			szam+=len(p.aa)
		return {"retval" : szam, "num_proteins" : pc}
	else:
		return {"retval" : "bad function", "num_proteins" : pc}

@app.route("/mongo/names", methods=["POST"])
def mongo():
	#req=request.get_json()
	#iter=int(request.args.get("iteration"))
	#reqi=int(req["iteration"])
	db = pymongo.MongoClient('localhost')["PYTHONCLASS2020"]["main"]
	l = []
	try:
		req=request.get_json()
		reqi=int(req["iteration"])
		for i in db.find({"iteration":reqi}):
			l.append(str(i["name"]))
		return jsonify(l)
	except TypeError:
		iter=int(request.args.get("iteration"))
		for i in db.find({"iteration":iter}):
			l.append(str(i["name"]))
		return jsonify(l)

@app.route("/mongo/clear", methods=["GET"])
def clear():
	#this should clean my collection every hour, delete everything

	my.delete_many({})

	return "clear"

@app.route("/mongo/create_user", methods=["POST"])
def mongo2():
	req=request.get_json()
	to_insert = {	"username" : str(req["username"]),
			"email": str(req["email"]),
			"password": str(req["password"]),
			"name": str(req["name"]) }
	my.insert_one(to_insert)
	un=str(request.args.get("username"))
	return str(req["username"])

@app.route("/mongo/check", methods=["POST"])  
def mongo4():
	return str(my.find_one({"username" : "frocli99"})) 

@app.route("/mongo/auth/names", methods=["POST"])
def mongo3():
	ln = []
	noln= []
	#un=str(request.args.get("username"))
	#pw=str(request.args.get("password"))
	req=request.get_json()
	ir=int(req["iteration"])
	un=str(req["username"])
	pw=str(req["password"])
	for i in main.find({"iteration":ir}):
		ln.append(str(i["name"]))

	if my.find_one({"username":un}) is not None:

		if my.find_one({"$and":[{"username":un},{"password":pw}]}) is not None:
			return {
		 	       	  "list_of_names": ln,
        	  		  "status": "ok"
  			       }
		else:
			tájp = type(my.find_one({"username":un}))
			return {      "list_of_names" : noln,
				      "status": "bad password" }

	else:
		dict = my.find_one({"username":"frocli99"})
		return 	 { 	"list_of_names" : noln,
				"status": "bad username" }

@app.route("/svm", methods=["POST"])
def iris():
	req=request.get_json()
	ipc=float(req["C"])
	ipg=float(req["gamma"])
	ipd=req["data"]
	from sklearn import datasets
	iris = datasets.load_iris()
	irisclf = svm.SVC(C=1, gamma=0.7, probability=True)
	irisclf.fit(iris.data,iris.target)
	faj = irisclf.predict(ipd)
	vegso = []
	faj = list(faj)
	sp = ""
	for i in range (len(faj)):
	    kaki = {
	        "probability" : 0,
	        "species" : "a"
	    }
	    if faj[i] == 1:
	        sp = "versicolor"
	    if faj[i] == 0:
	        sp = "setosa"
	    if faj[i] == 2:
	        sp = "virginica"
	    kaki["species"] = sp
	    kaki["probability"] = np.amax(irisclf.predict_proba([ipd[i]]))
	    vegso.append(kaki)

	return jsonify(vegso)

